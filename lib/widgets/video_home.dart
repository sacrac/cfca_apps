import 'package:chewie/chewie.dart';
//import 'package:chewie/src/chewie_player.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';


class VideoHome extends StatefulWidget {

  final VideoPlayerController videoPlayerController;
  final bool looping;

  VideoHome({
    required this.videoPlayerController,
    required this.looping,
    Key? key}) : super(key: key);

  @override
  _VideoHomState createState() => _VideoHomState();
}

class _VideoHomState extends State<VideoHome> {
  ChewieController? _chewieController2;

   @override
  void initState() {
    super.initState();
    _chewieController2 = ChewieController(
      videoPlayerController: widget.videoPlayerController,
      aspectRatio: 16 / 9,
      autoInitialize: true,
      looping: widget.looping,
      showControls: true,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: Chewie(
        controller: _chewieController2 as ChewieController,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    //widget.videoPlayerController.dispose();
    //_chewieController?.dispose();
  }


}