import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

import 'package:chewie/chewie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';
import 'package:permission_handler/permission_handler.dart';

class DownloadVideo extends StatefulWidget {
  final String videoString;
  final String videoName;

  const DownloadVideo({
    Key? key,
    required this.videoString,
    required this.videoName,
  }) : super(key: key);

  @override
  _DownloadVideoState createState() => _DownloadVideoState();
}

class _DownloadVideoState extends State<DownloadVideo> {

  bool downloading = false;
  var progressString = "";
  String? urlVideo;
  bool _btnEnabled = false;
  String txtBtnDescarga = "Descagar video";

  VideoPlayerController? _videoPlayerController;
  ChewieController? _chewieController;


  @override
  void initState() {
    super.initState();

    _loadPath();
  }

  void _loadPath() async {
    final prefs = await SharedPreferences.getInstance();

    urlVideo = (prefs.getString(widget.videoName) ?? '');

    final File fileDir = File(urlVideo!);

    _videoPlayerController = VideoPlayerController.file(fileDir);

    if (urlVideo != '') {
      await Future.wait([_videoPlayerController!.initialize()]);
      _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController!,
        autoPlay: false,
        looping: false,
        aspectRatio: 16 / 9,
      );
    }
    //setState(() {});
  }

  @override
  void dispose() {
    _videoPlayerController?.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  Future<void> download() async {
    //   downloadFile();
    var yt = YoutubeExplode();
    var id = widget.videoString;
    var video = await yt.videos.get(id);

    // Display info about this video.
    await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text('Title: ${video.title}, Duration: ${video.duration}'),
          actions: <Widget>[
            TextButton(
              child: const Text('Cerrar'),
              onPressed: () {
                Navigator.of(context).pop();
                //_btnEnabled = false;
              },
            ),
          ],
          
        );
      },
    );

    // Request permission to write in an external directory.
    // (In this case downloads)
    await Permission.storage.request();

    // Get the streams manifest and the audio track.
    var manifest = await yt.videos.streamsClient.getManifest(id);
    var audio = manifest.muxed.first;

    // Build the directory.
    var dir = await getApplicationDocumentsDirectory();
    var filePath =
        p.join(dir.uri.toFilePath(), '${video.id}.${audio.container.name}');

    // Open the file to write.
    var file = File(filePath);
    var fileStream = file.openWrite();

    // Pipe all the content of the stream into our file.
    var audioStream = yt.videos.streamsClient.get(audio);
    // Track the file download status.
    var len = audio.size.totalBytes;
    var count = 0;

    await for (final data in audioStream) {
      // Keep track of the current downloaded data.
      count += data.length;

      fileStream.add(data);

      setState(() {
        downloading = true;
        progressString = ((count / len) * 100).toStringAsFixed(0) + "%";
      });
    }
    //   await audioStream.pipe(fileStream);

    // Close the file.
    await fileStream.flush();
    await fileStream.close();

    final prefs = await SharedPreferences.getInstance();
    prefs.setString('video_'+widget.videoName, '$filePath');
    //print(prefs.getString('video_'+widget.videoName));

    final File fileDir = File('$filePath');
    _videoPlayerController = VideoPlayerController.file(fileDir);
    await Future.wait([_videoPlayerController!.initialize()]);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController!,
      autoPlay: false,
      looping: false,
      aspectRatio: 16 / 9,
    );

    setState(() {
      downloading = false;
      progressString = "Completed";
      urlVideo = '$filePath';
    });

  }



  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            
            Center(
              child: downloading
              ? Container(
                  height: 120.0,
                  width: 200.0,
                  child: Card(
                    color: Colors.black,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(
                          "Descargando Video: $progressString",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
              )
              : Container(
                  child: Center(
                      child: _chewieController != null &&
                              _chewieController!.videoPlayerController
                                  .value.isInitialized
                          ? Container(
                            width: 500,
                            height: 200,
                            child: Chewie(
                                controller: _chewieController!,
                              ),
                          )
                          : ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Color(0xFF7DAB82),
                              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                              textStyle: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(60.0)
                              )
                            ),
                            onPressed: ( _btnEnabled ) ? null : () async {
                              setState(() {
                                _btnEnabled = true;
                                txtBtnDescarga = "Espere un momento";
                              });
                              download();
                              
                            },
                            child: Text('$txtBtnDescarga')
                          ),
                    ),
              )
            ),
          ],
        ),
      );
  }
}