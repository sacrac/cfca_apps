// import 'package:animated_drawer/views/animated_drawer.dart';
// import 'package:cfca_apps/pages/home_page.dart';
// import 'package:flutter/material.dart';



// class MenuPage extends StatelessWidget {
//   const MenuPage({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return AnimatedDrawer(
//       homePageXValue: 150,
//       homePageYValue: 80,
//       homePageAngle: -0.2,
//       homePageSpeed: 250,
//       shadowXValue: 122,
//       shadowYValue: 110,
//       shadowAngle: -0.275,
//       shadowSpeed: 550,
//       openIcon: Icon(Icons.menu_open, color: Colors.black45, size: 35.0),
//       closeIcon: Icon(Icons.arrow_back_ios, color: Colors.black45, size: 35.0),
//       shadowColor: Color(0xFF416CA3),
//       backgroundGradient: LinearGradient(
//         colors: [Color(0xFF416CA3), Color(0xFF183E6F)],
//       ),
//       menuPageContent: Padding(
//         padding: const EdgeInsets.only(top: 100.0, left: 15),
//         child: Container(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               CircleAvatar(
//                 backgroundColor: Colors.white,
//                 radius: 50,
//                 child: Image.asset("assets/images/proyecto.png",
//                 height: 100.0,
//                 width: 100.0,
//                 ),
//               ),
//               Row(
//                 children: [
//                   Text(
//                     "Proyecto",
//                     style: TextStyle(
//                         fontSize: 17,
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold),
//                   ),
//                   Text(
//                     "CFCA",
//                     style: TextStyle(
//                         fontSize: 17,
//                         color: Colors.blue[200],
//                         fontWeight: FontWeight.bold),
//                   )
//                 ],
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 20),
//               ),
//               Divider(
//                 color: Color(0xFF416CA3),
//                 thickness: 2,
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 20),
//               ),
//               GestureDetector(
//                 onTap: (){
//                   Navigator.pushNamed(context, 'aprende');
//                 },
//                 child: Row(
//                   children: [
//                     Icon(Icons.school_rounded, color: Colors.white),
//                     SizedBox(width:10),
//                     Text(
//                       "Aprende",
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 17,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 30),
//               ),
//               GestureDetector(
//                 onTap: () {
//                   Navigator.pushNamed(context, 'directorio');
//                 },
//                 child: Row(
//                   children: [
//                     Icon(Icons.call, color: Colors.white),
//                     SizedBox(width:10),
//                     Text(
//                       "Directorio",
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 17,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 30),
//               ),
//               GestureDetector(
//                 onTap: () {
//                   Navigator.pushNamed(context, 'peticion');
//                 },
//                 child: Row(
//                   children: [
//                     Icon(Icons.record_voice_over_rounded, color: Colors.white),
//                     SizedBox(width:10),
//                     Text(
//                       "Peticiones",
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 17,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 30),
//               ),
//               GestureDetector(
//                 onTap: () {
//                   Navigator.pushNamed(context, 'mapa');
//                 },
//                 child: Row(
//                   children: [
//                     Icon(Icons.place_rounded, color: Colors.white),
//                     SizedBox(width:10),
//                     Text(
//                       "Mapa",
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 17,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 30),
//               ),
//               GestureDetector(
//                 onTap: () {
//                   Navigator.pushNamed(context, 'info');
//                 },
//                 child: Row(
//                   children: [
//                     Icon(Icons.info_rounded, color: Colors.white),
//                     SizedBox(width:10),
//                     Text(
//                       "acerca de",
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 17,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 30),
//               ),
//               Divider(
//                 color: Color(0xFF416CA3),
//                 thickness: 2,
//               ),
//               Spacer(),
//               Text(
//                 "Apoyan",
//                 style: TextStyle(
//                   color: Colors.white,
//                   fontWeight: FontWeight.bold,
//                   fontSize: 24.0
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 20),
//               ),
//               Image.asset("assets/images/todologos.png",
//               width: 250),
//               Padding(
//                 padding: EdgeInsets.only(bottom: 30),
//               ),
//             ],
//           ),
//         ),
//       ),
//       homePageContent: HomePage(),
//     );
//   }
// }