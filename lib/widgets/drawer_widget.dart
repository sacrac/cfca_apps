

import 'package:flutter/material.dart';

Widget menuDraver(BuildContext context) {
  return Drawer(
    child: Column(
      children: <Widget>[
        DrawerHeader(
          child: Row(
                  children: [
                   CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 50,
                    child: Image.asset("assets/images/proyecto.png",
                      height: 100.0,
                      width: 100.0,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Proyecto",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "CFCA",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.blue[200],
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
          decoration: BoxDecoration(
            color: Colors.blue,
            image: DecorationImage(
              image: AssetImage("assets/images/foto_drawer1.jpg"),
              fit: BoxFit.cover,
              colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
              )
          ),
        ),
        ListTile(
          leading: Icon(Icons.home_rounded),
          title: Text('Inicio'),
          onTap: () {
             Navigator.pushNamed(context, '/');
          },
        ),
        ListTile(
          leading: Icon(Icons.school_rounded),
          title: Text('Aprende'),
          onTap: () {
             Navigator.pushNamed(context, 'aprende');
          },
        ),
        ListTile(
          leading: Icon(Icons.call),
          title: Text('Directorio'),
          onTap: () {
             Navigator.pushNamed(context, 'directorio');
          },
        ),
        // ListTile(
        //   leading: Icon(Icons.record_voice_over_rounded),
        //   title: Text('Peticiones'),
        //   onTap: () {
        //      Navigator.pushNamed(context, 'peticion');
        //   },
        // ),
        ListTile(
          leading: Icon(Icons.place_rounded),
          title: Text('Mapa'),
          onTap: () {
             Navigator.pushNamed(context, 'mapa_info');
          },
        ),
        ListTile(
          leading: Icon(Icons.info_rounded),
          title: Text('Info'),
          onTap: () {
             Navigator.pushNamed(context, 'info');
          },
        ),
        Divider(
          color: Color(0xFFC7C7C7),
          thickness: 2,
        ),
        
        Expanded(child: Container(),),
        Container(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Text(
            "La app es una iniciativa promovida por",
            style: TextStyle(
              color: Colors.black26,
              fontWeight: FontWeight.bold,
              fontSize: 19.0
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Image.asset("assets/images/todologos.png",
          width: 250),
        ),
        SizedBox(height: 15),

      ],
    ),
  );
}