import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/repositories/curso_repository.dart';
import 'package:meta/meta.dart';

part 'cursos_event.dart';
part 'cursos_state.dart';

class CursosBloc extends Bloc<CursosEvent, CursosState> {
  final CursoRepository cursoRepo;

  CursosBloc({required this.cursoRepo}) : super(CursoInitial());

  @override
  Stream<CursosState> mapEventToState( CursosEvent event ) async* {
    if ( event is CursosRequest ) {
      try {
        yield CursoLoadInProgress();
        await Future.delayed(const Duration(seconds: 1));
        //cursoRepo.cursoApi.getCursos();
        final List<Curso> cursos = await cursoRepo.getAllCursos();
        yield CursoLoadSuccess(curso: cursos);
      } catch ( _ ) {
        yield CursoLoadFailure();
      }
    }

  }
}
