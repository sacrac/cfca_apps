part of 'cursos_bloc.dart';

@immutable
abstract class CursosState {}

class CursoInitial extends CursosState {}

class CursoLoadInProgress extends CursosState {}

class CursoLoadSuccess extends CursosState {
  final List<Curso> curso;

  CursoLoadSuccess({required this.curso});


}

class CursoLoadFailure extends CursosState {}