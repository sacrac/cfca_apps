import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cfca_apps/models/directorio_model.dart';
import 'package:cfca_apps/repositories/directorio_repository.dart';
//import 'package:cfca_apps/services/directorio_service.dart';
import 'package:meta/meta.dart';

part 'directorio_event.dart';
part 'directorio_state.dart';

class DirectorioBloc extends Bloc<DirectorioEvent, DirectorioState> {
  final DirectorioRepository dirRepo;

  DirectorioBloc({required this.dirRepo}) : super(DirectorioInitial());

  @override
  Stream<DirectorioState> mapEventToState( DirectorioEvent event ) async* {
    if ( event is DirectorioRequest ) {
      try {
        yield DirectorioLoadInProgress();
        await Future.delayed(const Duration(seconds: 1));
        //dirRepo.directorioApi.getDirectorios();
        final List<Directorio> directorio = await dirRepo.getAllDirectorio();
        yield DirectorioLoadSuccess(directorio: directorio);
      } catch ( _ ) {
        yield DirectorioLoadFailure();
      }
    }
  }


}

// class UserBLoC {
//   Stream<List<Directorio>> get usersList async* {
//     yield await DirectorioService().getDirectorios();
//   }

//   final StreamController<int> _userCounter = StreamController<int>();

//   Stream<int> get userCounter => _userCounter.stream;

//   UserBLoC() {
//     usersList.listen((list) => _userCounter.add(list.length));
//   }
// }
