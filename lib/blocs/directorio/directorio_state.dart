part of 'directorio_bloc.dart';

@immutable
abstract class DirectorioState {}

class DirectorioInitial extends DirectorioState {}

class DirectorioLoadInProgress extends DirectorioState {}

class DirectorioLoadSuccess extends DirectorioState {
  final List<Directorio> directorio;

  DirectorioLoadSuccess({required this.directorio});


}

class DirectorioLoadFailure extends DirectorioState {}