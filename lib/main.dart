import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:cfca_apps/simple_bloc_observer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:cfca_apps/blocs/networkbloc/network_bloc.dart';
import 'package:cfca_apps/settings/router.dart';


import 'package:cfca_apps/blocs/directorio/directorio_bloc.dart';
import 'blocs/cursos/cursos_bloc.dart';

import 'package:cfca_apps/repositories/directorio_repository.dart';
import 'package:cfca_apps/repositories/curso_repository.dart';

import 'package:cfca_apps/services/modulo_service.dart';
import 'package:cfca_apps/services/contenido_service.dart';
import 'package:cfca_apps/services/directorio_service.dart';
import 'package:cfca_apps/services/cursos_service.dart';


class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
        ..maxConnectionsPerHost = 15;
  }
}


void main() {
  //Bloc.observer = SimpleBlocObserver();
  HttpOverrides.global = MyHttpOverrides();
  DirectorioService().getDirectorios();
  CursoService().getCursos();
  ModuloService().getModulos();
  ContenidoService().getContenidos();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        //BlocProvider<NetworkBloc>(create: ( _ ) => NetworkBloc()..add(ListenConnection())),
        BlocProvider<DirectorioBloc>(create: ( _ ) => DirectorioBloc(dirRepo: DirectorioRepository())),
        BlocProvider<CursosBloc>(create: ( _ ) => CursosBloc(cursoRepo: CursoRepository())),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
        //initialRoute: '/',
        routes: getRouter(),
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
 @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final splashDelay = 5;

  @override
  void initState() {
    super.initState();

    _loadWidget();
  }

  _loadWidget() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _introSeen = (prefs.getBool('intro_seen') ?? false);
    if (_introSeen) {
      var _duration = Duration(seconds: 2);
      return Timer(_duration, checkFirstSeen);
    }else{
      var _duration = Duration(seconds: splashDelay);
      return Timer(_duration, checkFirstSeen);
    }
    
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _introSeen = (prefs.getBool('intro_seen') ?? false);
    
    Navigator.pop(context);
    if (_introSeen) {
      Navigator.pushNamed(context, 'home');
    } else {
      await prefs.setBool('intro_seen', true);
      Navigator.pushNamed(context, 'home');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(child: CircularProgressIndicator()),
        ]
      ),
    );
  }
}