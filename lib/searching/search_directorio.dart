//import 'package:cfca_apps/blocs/directorio/directorio_bloc.dart';
import 'package:flutter/material.dart';
import 'package:cfca_apps/pages/directorio_page.dart';
import 'package:cfca_apps/dao/directorio_dao.dart';
import 'package:cfca_apps/models/directorio_model.dart';



class BuscadorPersona extends SearchDelegate<Directorio?> {

  @override
  final String searchFieldLabel;

  final directorioDao = DirectorioDao();

  BuscadorPersona(this.searchFieldLabel);


  @override
  List<Widget> buildActions(BuildContext context) {
      return [
        IconButton(
          icon: Icon(Icons.clear_sharp),
          onPressed: (){
            //print("Presione el accion!");
            query = '';
          }
        )
      ];
    }

    @override
    Widget buildLeading(BuildContext context) {
      return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: (){
          close(context, null);
        }
      );
    }

    @override
    Widget buildResults(BuildContext context) {
      //por si alguien da enter
      if ( query.trim().length == 0 ) {
        return Center(child: Text("Por favor escriba algo para buscar!"));
      }
      return FutureBuilder<List<Directorio>>(
      future: directorioDao.findAllDirectorio(query),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        if ( snapshot.hasError ) {
          return ListTile(
            title: Text("Busqueda no encontrada"),
          );
        }
        return snapshot.hasData
              ? DirectorioList(directorios: snapshot.data as List<Directorio>)
              : Center(child: CircularProgressIndicator());

      }
    );
    }

    @override
    Widget buildSuggestions(BuildContext context) {

      if (query.isEmpty || query.length == 0) {
        return Center(
          child: Text("Aun no busca nada!"),
        );
      }

      return FutureBuilder<List<Directorio>>(
      future: directorioDao.findAllDirectorio(query),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        // ignore: unnecessary_null_comparison
        if ( snapshot.connectionState == ConnectionState.none && snapshot.hasData == null ) {
          return Center(child: CircularProgressIndicator());
        }
        return snapshot.hasData
              ? DirectorioList(directorios: snapshot.data as List<Directorio>)
              : Center(child: CircularProgressIndicator());

      }
    );

  }

}