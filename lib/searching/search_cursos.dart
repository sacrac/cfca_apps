import 'package:cfca_apps/dao/curso_dao.dart';
import 'package:cfca_apps/pages/aprende_page.dart';
import 'package:flutter/material.dart';
import 'package:cfca_apps/models/curso_model.dart';




class BuscadorCurso extends SearchDelegate<Curso?> {

  @override
  final String searchFieldLabel;

  final cursoDao = CursoDao();

  BuscadorCurso(this.searchFieldLabel);


  @override
  List<Widget> buildActions(BuildContext context) {
      return [
        IconButton(
          icon: Icon(Icons.clear_sharp),
          onPressed: (){
            //print("Presione el accion!");
            query = '';
          }
        )
      ];
    }

    @override
    Widget buildLeading(BuildContext context) {
      return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: (){
          close(context, null);
        }
      );
    }

    @override
    Widget buildResults(BuildContext context) {
      //por si alguien da enter
      if ( query.trim().length == 0 ) {
        return Center(child: Text("Por favor escriba algo para buscar!"));
      }
      return FutureBuilder<List<Curso>>(
      future: CursoDao().findAllCursos(query),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        if ( snapshot.hasError ) {
          return ListTile(
            title: Text("Busqueda no encontrada"),
          );
        }
        return snapshot.hasData
              ? CursoList(cursos: snapshot.data as List<Curso>)
              : Center(child: CircularProgressIndicator());

      }
    );
    }

    @override
    Widget buildSuggestions(BuildContext context) {

      if (query.isEmpty || query.length == 0) {
        return Center(
          child: Text("Aun no busca nada!"),
        );
      }

      return FutureBuilder<List<Curso>>(
      future: CursoDao().findAllCursos(query),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot){
        if ( snapshot.connectionState == ConnectionState.none ) {
          return Center(child: CircularProgressIndicator());
        }
        return snapshot.hasData
              ? CursoList(cursos: snapshot.data as List<Curso>)
              : Center(child: CircularProgressIndicator());

      }
    );

  }

}