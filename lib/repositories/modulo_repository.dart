import 'package:cfca_apps/dao/modulo_dao.dart';
import 'package:cfca_apps/models/modulo_model.dart';
import 'package:cfca_apps/services/modulo_service.dart';

class ModuloRepository {

  final moduloDao = ModuloDao();
  final moduloApi = ModuloService();

  Future getAllModulos(int id) => moduloDao.getAllModulo(id);
  Future getModulo() => moduloApi.getModulos();
  Future getModuloId(int id) => moduloDao.getModuloId(id);
  Future insertModulo(Modulo modulo) => moduloDao.createModulo(modulo);
  Future updateModulo(Modulo modulo) => moduloDao.updateModulo(modulo);
  Future deleteModuloById(int id) => moduloDao.deleteModulo(id);

}