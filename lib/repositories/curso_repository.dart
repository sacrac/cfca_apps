import 'package:cfca_apps/dao/curso_dao.dart';
import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/services/cursos_service.dart';

class CursoRepository {

  final cursoDao = CursoDao();
  final cursoApi = CursoService();

  Future<List<Curso>> getAllCursos() => cursoDao.getAllCursos();
  Future getCurso() => cursoApi.getCursos();
  Future getCursoId(int id) => cursoDao.getCursoId(id);
  Future insertCurso(Curso curso) => cursoDao.createCurso(curso);
  Future updateCurso(Curso curso) => cursoDao.updateCurso(curso);
  Future deleteCursoById(int id) => cursoDao.deleteCurso(id);

}