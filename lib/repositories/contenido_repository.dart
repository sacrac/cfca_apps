import 'package:cfca_apps/dao/contenido_dao.dart';
import 'package:cfca_apps/models/contenido_model.dart';
import 'package:cfca_apps/services/contenido_service.dart';

class ContenidoRepository {

  final contenidoDao = ContenidoDao();
  final contenidoApi = ContenidoService();

  Future getAllContenidos(List ids) => contenidoDao.getAllContenidos(ids as List<int>);
  Future getContenido() => contenidoApi.getContenidos();
  Future getContenidoId(int id) => contenidoDao.getContenidoId(id);
  Future insertContenido(Contenido contenido) => contenidoDao.createContenido(contenido);
  Future updateContenido(Contenido contenido) => contenidoDao.updateContenido(contenido);
  Future deleteContenidoById(int id) => contenidoDao.deleteContenido(id);

}