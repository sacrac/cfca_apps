
import 'package:cfca_apps/dao/directorio_dao.dart';
import 'package:cfca_apps/models/directorio_model.dart';
import 'package:cfca_apps/services/directorio_service.dart';

class DirectorioRepository {

  final directorioDao = DirectorioDao();
  final directorioApi = DirectorioService();

  Future getAllDirectorio() => directorioDao.getAllDirectorio();
  Future getDirectorio() => directorioApi.getDirectorios();
  Future insertPersona(Directorio persona) => directorioDao.createDirectorio(persona);
  Future updatePersona(Directorio persona) => directorioDao.updateDirectorio(persona);
  Future deletePersonaById(int id) => directorioDao.deletePersona(id);

}