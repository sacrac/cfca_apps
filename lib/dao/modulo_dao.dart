
import 'package:cfca_apps/models/modulo_model.dart';
import 'package:cfca_apps/settings/database.dart';

class ModuloDao {

  final dbProvider = DBHelper.db;

  //agregamos un nuevo registro modulo
  createModulo(Modulo newModulo) async {
    final db = await dbProvider.database;
    final qexist = await db!.query('Modulo', where: 'id = ?', whereArgs: [newModulo.id]);
    if (qexist.isNotEmpty) {
      print("Existe el modulo");
      final flag = await db.query('Modulo',
          where: 'id = ? AND modificado = ?',
          whereArgs: [newModulo.id, newModulo.modificado]);

      if (!flag.isNotEmpty) {
        final res = await db.update('Modulo', newModulo.toJson(),
            where: 'id = ?', whereArgs: [newModulo.id]);
        // print('se actualizo el id: $newModulo.id');
        return res;
      }
    } else {
      final res =  db.insert('Modulo', newModulo.toJson());
      return res;
        }
  }

  //buscamos todos los registro
  Future<List<Modulo>> getAllModulo(int id) async {
    final db = await dbProvider.database;
    final res = await db!.query('Modulo', where: 'curso_id = ?', whereArgs: [id], orderBy: '"order" ASC');
    List<Modulo> list =
        res.isNotEmpty ? res.map((c) => Modulo.fromJson(c)).toList() : [];

    return list;
  }

  Future<int> conteoModulosPages(int id) async {
    final db = await dbProvider.database;
    final count = await db!.rawQuery(
        "SELECT COUNT(*) as total from Modulo WHERE curso_id=?", [id]);
    return count[0]['total'] as int;
  }

  //buscar un registro por id
  Future<List<Modulo>> findAllModulos(String query) async {
    final db = await dbProvider.database;
    final res = await db!.rawQuery( "SELECT * FROM Modulo WHERE titulo LIKE '%$query%' " );
    List<Modulo> list =
        res.isNotEmpty ? res.map((c) => Modulo.fromJson(c)).toList() : [];

    return list;
  }

  //actualizamos un registro
  Future<int> updateModulo(Modulo modulo) async {
    final db = await dbProvider.database;
    var result = await db!.update('Modulo', modulo.toJson(),
        where: "id = ?", whereArgs: [modulo.id]);
    return result;
  }

  //eliminamos un registro
  Future<int> deleteModulo(int id) async {
    final db = await dbProvider.database;
    var result = await db!.delete('Modulo', where: 'id = ?', whereArgs: [id]);
    return result;
  }

   //obtener un registro
  Future<Modulo?> getModuloId(int id) async{
    final db = await dbProvider.database;
    final res = await db!.query('Modulo', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? Modulo.fromJson(res.first) : null;
  }



}