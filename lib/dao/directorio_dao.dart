

import 'package:cfca_apps/models/directorio_model.dart';
import 'package:cfca_apps/settings/database.dart';

class DirectorioDao {

  final dbProvider = DBHelper.db;

  //agregamos un registro del directorio
  createDirectorio(Directorio newPersona) async {
    final db = await dbProvider.database;
    final qexist = await db!.query('Directorio', where: 'id = ?', whereArgs: [newPersona.id]);
    if (qexist.isNotEmpty) {
      print("Existe el contacto");
      final flag = await db.query('Directorio',
          where: 'id = ? AND modificado = ?',
          whereArgs: [newPersona.id, newPersona.modificado]);

      if (!flag.isNotEmpty) {
        final res = await db.update('Directorio', newPersona.toJson(),
            where: 'id = ?', whereArgs: [newPersona.id]);
        // print('se actualizo el id: $newPersona.id');
        return res;
      }
    } else {
      final res =  db.insert('Directorio', newPersona.toJson());
      return res;
    }
  }

  //buscamos todos los registro
  Future<List<Directorio>> getAllDirectorio() async {
    final db = await dbProvider.database;
    final res = await db!.rawQuery("SELECT * FROM Directorio");
    List<Directorio> list =
        res.isNotEmpty ? res.map((c) => Directorio.fromJsonMap(c)).toList() : [];

    return list;
  }

  //buscar un registro por id
  Future<List<Directorio>> findAllDirectorio(String query) async {
    final db = await dbProvider.database;
    final res = await db!.rawQuery( " SELECT * FROM Directorio WHERE nombre_persona LIKE '%$query%' OR tema LIKE '%$query%' OR municipio LIKE '%$query%' " );
    List<Directorio> list =
        res.isNotEmpty ? res.map((c) => Directorio.fromJsonMap(c)).toList() : [];

    return list;
  }

  //actualizamos un registro
  Future<int> updateDirectorio(Directorio persona) async {
    final db = await dbProvider.database;
    var result = await db!.update('Directorio', persona.toJson(),
        where: "id = ?", whereArgs: [persona.id]);
    return result;
  }

  //eliminamos un registro
  Future<int> deletePersona(int id) async {
    final db = await dbProvider.database;
    var result = await db!.delete('Directorio', where: 'id = ?', whereArgs: [id]);
    return result;
  }



}