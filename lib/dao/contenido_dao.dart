
import 'package:cfca_apps/models/contenido_model.dart';
import 'package:cfca_apps/settings/database.dart';

class ContenidoDao {

  final dbProvider = DBHelper.db;

  //agregamos un nuevo registro contenido
  createContenido(Contenido newContenido) async {
    final db = await dbProvider.database;
    final qexist = await db!.query('Contenido', where: 'id = ?', whereArgs: [newContenido.id]);
    if (qexist.isNotEmpty) {
      print("Existe el contenido");
       final flag = await db.query('Contenido',
          where: 'id = ? AND modificado = ?',
          whereArgs: [newContenido.id, newContenido.modificado]);

      if (!flag.isNotEmpty) {
        final res = await db.update('Contenido', newContenido.toJson(),
            where: 'id = ?', whereArgs: [newContenido.id]);
        // print('se actualizo el id: $newContenido.id');
        return res;
      }
    } else {
      final res =  db.insert('Contenido', newContenido.toJson());
      return res;
        }
  }

  //buscamos todos los registro contenidos
  // Future<List<Contenido>> getAllContenidos() async {
  //   final db = await dbProvider.database;
  //   final res = await db.rawQuery("SELECT * FROM Contenido");
  //   List<Contenido> list =
  //       res.isNotEmpty ? res.map((c) => Contenido.fromJson(c)).toList() : [];

  //   return list;
  // }
  Future getAllContenidos(List<int> idsModulos) async{
    final db = await dbProvider.database;
    String myQuery= 'SELECT * FROM Contenido WHERE modulo_id IN $idsModulos ORDER BY "order" ASC' ;
    String limpiar= myQuery.replaceAll('[', '(');
    myQuery= limpiar.replaceAll(']', ')');
    final res = await db!.rawQuery(myQuery);
    List <Contenido> list = res.isNotEmpty
                                ? res.map((e) => Contenido.fromJson(e)).toList()
                                : [];
    return list;
  }

  Future<int> conteoContenido(int id) async {
    final db = await dbProvider.database;
    final count = await db!.rawQuery(
        "SELECT COUNT(*) as total from Contenido WHERE modulo_id=?", [id]);
    return count[0]['total'] as int;
  }

  Future<int> conteoContenidosPages(int id) async {
    final db = await dbProvider.database;
    final count = await db!.rawQuery(
        "SELECT COUNT(*) as total FROM Curso ac INNER JOIN Modulo am ON ac.id = am.curso_id INNER JOIN Contenido ap ON ap.modulo_id = am.id WHERE curso_id=?", [id]);
    return count[0]['total'] as int;
  }


  //buscar un registro por id
  Future<List<Contenido>> findAllContenidos(String query) async {
    final db = await dbProvider.database;
    final res = await db!.rawQuery( " SELECT * FROM Contenido WHERE titulo LIKE '%$query%' " );
    List<Contenido> list =
        res.isNotEmpty ? res.map((c) => Contenido.fromJson(c)).toList() : [];

    return list;
  }

  //actualizamos un registro
  Future<int> updateContenido(Contenido contenido) async {
    final db = await dbProvider.database;
    var result = await db!.update('Contenido', contenido.toJson(),
        where: "id = ?", whereArgs: [contenido.id]);
    return result;
  }

  //eliminamos un registro
  Future<int> deleteContenido(int id) async {
    final db = await dbProvider.database;
    var result = await db!.delete('Contenido', where: 'id = ?', whereArgs: [id]);
    return result;
  }

   //eliminamos un registro
  Future<Contenido?> getContenidoId(int id) async{
    final db = await dbProvider.database;
    final res = await db!.query('Contenido', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? Contenido.fromJson(res.first) : null;
  }



}