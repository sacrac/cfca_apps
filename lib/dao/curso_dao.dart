
import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/settings/database.dart';

class CursoDao {

  final dbProvider = DBHelper.db;

  //agregamos un nuevo registro curso
  createCurso(Curso newCurso) async {
    final db = await dbProvider.database;
    final qexist = await db!.query('Curso', where: 'id = ?', whereArgs: [newCurso.id]);
    if (qexist.isNotEmpty) {
      print("Existe el curso");
      final flag = await db.query('Curso',
          where: 'id = ? AND modificado = ?',
          whereArgs: [newCurso.id, newCurso.modificado]);

      if (!flag.isNotEmpty) {
        final res = await db.update('Curso', newCurso.toJson(),
            where: 'id = ?', whereArgs: [newCurso.id]);
        // print('se actualizo el id: $newCurso.id');
        return res;
      }
    } else {
      final res =  db.insert('Curso', newCurso.toJson());
      return res;
        }
  }

  //buscamos todos los registro
  Future<List<Curso>> getAllCursos() async {
    final db = await dbProvider.database;
    final res = await db!.query("Curso");
    List<Curso> list = res.isNotEmpty 
                      ? res.map((c) => Curso.fromJson(c)).toList() 
                      : [];

    return list;
  }

  //buscar un registro por id
  Future<List<Curso>> findAllCursos(String query) async {
    final db = await dbProvider.database;
    final res = await db!.rawQuery( " SELECT * FROM Curso WHERE titulo LIKE '%$query%' OR descripcion LIKE '%$query%' " );
    List<Curso> list =
        res.isNotEmpty ? res.map((c) => Curso.fromJson(c)).toList() : [];

    return list;
  }

  //actualizamos un registro
  Future<int> updateCurso(Curso curso) async {
    final db = await dbProvider.database;
    var result = await db!.update('Curso', curso.toJson(),
        where: "id = ?", whereArgs: [curso.id]);
    return result;
  }

  //eliminamos un registro
  Future<int> deleteCurso(int id) async {
    final db = await dbProvider.database;
    var result = await db!.delete('Curso', where: 'id = ?', whereArgs: [id]);
    return result;
  }

   //eliminamos un registro
  Future<Curso?> getCursoId(int id) async{
    final db = await dbProvider.database;
    final res = await db!.query('Curso', where: 'id = ?', whereArgs: [id]);
    return res.isNotEmpty ? Curso.fromJson(res.first) : null;
  }



}