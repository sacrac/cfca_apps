import 'dart:convert';

import 'package:cfca_apps/dao/curso_dao.dart';
import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/repositories/curso_repository.dart';
import 'package:http/http.dart' as http;
import 'package:cfca_apps/settings/apis.dart';


class CursoService {
  CursoRepository? cursoRepository;
  String _url = 'caps-nicaragua.org';

  Future<List<Curso>> getCursos() async {

    final url = Uri.https(_url, ApiConstant.aprende_cursos_api);
    List collection;
    List<Curso> _cursos = [];

    final resp = await http.get(url);
    if (resp.statusCode == 200) {
      final dataRaw = utf8.decode(resp.bodyBytes);
      collection = json.decode(dataRaw);
      _cursos = collection.map((json) {
        CursoDao().createCurso(Curso.fromJson(json));
        return Curso.fromJson(json);
      }).toList();
    } else {
      print('Solicitud fallada con status: ${resp.statusCode}.');
    }

    return _cursos;
  }

}