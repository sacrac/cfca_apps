import 'dart:convert';
import 'package:cfca_apps/dao/modulo_dao.dart';
import 'package:http/http.dart' as http;

import 'package:cfca_apps/models/modulo_model.dart';
import 'package:cfca_apps/repositories/modulo_repository.dart';
import 'package:cfca_apps/settings/apis.dart';



class ModuloService {
  ModuloRepository? moduloRepository;
  String _url = 'caps-nicaragua.org';

  Future<List<Modulo>> getModulos() async {

    final url = Uri.https(_url, ApiConstant.aprende_modulos_api);
    List collection;
    List<Modulo> _modulos = [];

    final resp = await http.get(url);
    if (resp.statusCode == 200) {
      final dataRaw = utf8.decode(resp.bodyBytes);
      collection = json.decode(dataRaw);
      _modulos = collection.map((json) {
        ModuloDao().createModulo(Modulo.fromJson(json));
        return Modulo.fromJson(json);
      }).toList();
    } else {
      print('Solicitud fallada con status: ${resp.statusCode}.');
    }

    return _modulos;
  }

}