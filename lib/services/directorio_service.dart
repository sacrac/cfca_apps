import 'dart:convert';

import 'package:cfca_apps/dao/directorio_dao.dart';
import 'package:cfca_apps/repositories/directorio_repository.dart';
import 'package:http/http.dart' as http;
import 'package:cfca_apps/settings/apis.dart';
import 'package:cfca_apps/models/directorio_model.dart';


class DirectorioService {
  DirectorioRepository? directorioRepository;
  String _url = 'caps-nicaragua.org';

  Future<List<Directorio>> getDirectorios() async {

    final url = Uri.https(_url, ApiConstant.directorio_api);
    List collection;
    List<Directorio> _contacts = [];

    final resp = await http.get(url);
    if (resp.statusCode == 200) {
      final dataRaw = utf8.decode(resp.bodyBytes);
      collection = json.decode(dataRaw);
      _contacts = collection.map((json) {
        DirectorioDao().createDirectorio(Directorio.fromJsonMap(json));
        return Directorio.fromJsonMap(json);
      }).toList();
    } else {
      print('Solicitud fallada con status: ${resp.statusCode}.');
    }

    return _contacts;
  }

}