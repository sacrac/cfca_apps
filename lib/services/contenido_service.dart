import 'dart:convert';

import 'package:cfca_apps/dao/contenido_dao.dart';
import 'package:cfca_apps/models/contenido_model.dart';
import 'package:cfca_apps/repositories/contenido_repository.dart';
import 'package:http/http.dart' as http;
import 'package:cfca_apps/settings/apis.dart';


class ContenidoService {
  ContenidoRepository? contenidoRepository;
  String _url = 'caps-nicaragua.org';

  Future<List<Contenido>> getContenidos() async {

    final url = Uri.https(_url, ApiConstant.aprende_contenidos_api);
    List collection;
    List<Contenido> _contenido = [];

    final resp = await http.get(url);
    if (resp.statusCode == 200) {
      final dataRaw = utf8.decode(resp.bodyBytes);
      collection = json.decode(dataRaw);
      _contenido = collection.map((json) {
        ContenidoDao().createContenido(Contenido.fromJson(json));
        return Contenido.fromJson(json);
      }).toList();
    } else {
      print('Solicitud fallada con status: ${resp.statusCode}.');
    }

    return _contenido;
  }

}