//import 'dart:ffi';
import 'dart:io';
import 'package:flutter/material.dart';

import 'package:cfca_apps/widgets/download_video.dart';
import 'package:cfca_apps/widgets/video_home.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';


import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/repositories/curso_repository.dart';
import 'package:cfca_apps/searching/search_cursos.dart';


const youtubeUrl = 'https://www.youtube.com/channel/UCqEBGraEA_nT5M9OhZnGBvA/videos';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
      width: width,
      height: height,
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 50),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(width: 80),
                Text("CAPS - Nicaragua",
                  style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold, color: Color(0xFF7C7C7C))),
                SizedBox(width: 80),
                GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'info'),
                  child: Icon(Icons.info, color: Color(0xFF2B4A6F),)
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 30),
            ),
            Container(
              padding: EdgeInsets.symmetric( horizontal: 30),
              width:  MediaQuery.of(context).size.width,
              child: GestureDetector(
                onTap: () {
                  showSearch(context: context, delegate: BuscadorCurso('Buscar...') );
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 13),
                  width: double.infinity,
                  child: Row(
                    children: [
                      Icon(Icons.search, color: Color(0xFF999999),),
                      SizedBox(width: 30),
                      Text("Buscas un curso en particular?", style: TextStyle(color: Color(0xFF999999)),)
                    ],
                  ),
                  decoration: BoxDecoration(
                    color:  Color(0xFFE7E7E7),
                    borderRadius: BorderRadius.circular(100),

                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 30),
            ),
            Row(
              children: [
                SizedBox(width: 35),
                Text("Explorar Aplicaciones",
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Color(0xFF999999))),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(width:22),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, 'aprende');
                      },
                      child: _card(
                        primary: Color(0xFF2B4A6F),
                        chipText: "Aprende",
                        icono: Icon(Icons.school_rounded, color: Color(0xFF2B4A6F),size: 37,)
                        ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, 'directorio');
                      },
                      child: _card(
                        primary: Color(0xFFAA7E38),
                        chipText: "Directorio",
                        icono: Icon(Icons.call, color: Color(0xFFAA7E38), size: 37,)
                        ),
                    ),
                    // GestureDetector(
                    //   onTap: (){
                    //     Navigator.pushNamed(context, 'peticion');
                    //   },
                    //   child: _card(
                    //     primary: Color(0xFF393276),
                    //     chipText: "Peticiones",
                    //     icono: Icon(Icons.record_voice_over_rounded, color: Color(0xFF393276), size: 34,)
                    //     ),
                    // ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, 'mapa_info');
                      },
                      child: _card(
                        primary: Color(0xFFAA9138),
                        chipText: "Mapa",
                        icono: Icon(Icons.place_rounded, color: Color(0xFFAA9138), size: 37,)
                        ),
                    ),
                  ]
                )
              )
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
            Row(
              children: [
                SizedBox(width: 35),
                Text("Videos sobre el agua",
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Color(0xFF999999))),
                Spacer(),
                GestureDetector(
                  onTap: launchURL,
                  child: Text("Más videos",
                    style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold, color: Color(0xFF2B4A6F))
                  ),
                ),
                SizedBox(width: 35),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
            Container(
              width: double.infinity,
              height: 200,
              child: FutureBuilder(
                  future: _comprobarVideo('path'),
                  //initialData: InitialData,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if ( snapshot.hasData) {
                      
                      if ( snapshot.data[0] ) {
                        
                        return Container(
                          width: 500,
                          height: 200,
                          child: VideoHome(
                            looping: false,
                            videoPlayerController: VideoPlayerController.file(File(snapshot.data[1]))),
                        );


                      }
                      else {
                        return DownloadVideo(videoString: 'https://www.youtube.com/watch?v=yxRbOK86QKA', videoName: 'path');
                      }
                    } else {
                      print("NO hay video");
                    }
                    return Container();
                  },
                )
            ),
            
            Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
            Row(
              children: [
                SizedBox(width: 35),
                Text("Cursos",
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Color(0xFF999999))),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            _cursosTotales(),

          ],
          ),
      ),
      ),
    );
  }

  _comprobarVideo(String videoName) async {
    final prefs = await SharedPreferences.getInstance();
    bool existe = false;
    var ruta = prefs.getString('video_$videoName');
    if ( ruta != null ) {
      existe = true;
    } else {
      existe = false;
    }
    return [existe, ruta];
  }


  void launchURL() async =>
    await canLaunch(youtubeUrl) ? await launch(youtubeUrl) : throw 'Could not launch $youtubeUrl';

  Widget _card({Color primary = Colors.redAccent,
                String chipText = '', Icon? icono,
                }) {
    return Container(
        height: 105,
        width: 100,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 13),
        decoration: BoxDecoration(
            color: primary.withAlpha(200),
            borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                Center(
                    child: CircleAvatar(
                      backgroundColor: Color(0xFFC4C4C4),
                      maxRadius: 22,
                      child: Container(child: icono,),
                    )),
                SizedBox(height: 8.0),
                Center(
                  child: _cardInfo(chipText),
                )
              ],
            ),
          ),
        ));
  }

  Widget _cardInfo(String textapp) {
    return Column(
      children: <Widget>[
        SizedBox(height: 5),
        Text(textapp, style: TextStyle(color: Colors.white),)
      ],
    );
  }

  Widget _cursosTotales() {
    final cursosTotales = CursoRepository().getAllCursos();
    
    return FutureBuilder(
      future: cursosTotales,
      builder: (BuildContext context, AsyncSnapshot<List<Curso>>  snapshot) {
        if ( snapshot.data != null ) {
          final data = snapshot.data;
          return CarouselSlider(
                options: CarouselOptions(
                  height: 200.0,
                  autoPlay: true,
                  aspectRatio: 2.0,
                  enlargeCenterPage: true),
                items: data!.map((index) {
                  return Builder(
                    builder: (BuildContext context) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, 'aprende_detail', arguments: [index.id, index.titulo, index]);
                        },
                        child: Container(
                          margin: EdgeInsets.all(5.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            child: Stack(
                              children: <Widget>[
                                CachedNetworkImage(
                                  key: UniqueKey(),
                                  imageUrl: index.imagen as String, fit: BoxFit.cover, width: 1000.0),
                                Positioned(
                                  bottom: 0.0,
                                  left: 0.0,
                                  right: 0.0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Color.fromARGB(200, 0, 0, 0),
                                          Color.fromARGB(0, 0, 0, 0)
                                        ],
                                        begin: Alignment.bottomCenter,
                                        end: Alignment.topCenter,
                                      ),
                                    ),
                                    padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                                    child: Text(
                                      index.titulo as String,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ),
                        ),
                      );
                    },
                  );
                }).toList(),
          );

        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }
}