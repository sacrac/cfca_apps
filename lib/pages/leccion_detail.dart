import 'dart:io';

//import 'package:cfca_apps/settings/utils.dart';
import 'package:cfca_apps/widgets/download_video.dart';
import 'package:cfca_apps/widgets/video_home.dart';
//import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/html_parser.dart';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

//import 'package:flutter_youtube_downloader/flutter_youtube_downloader.dart';

import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/models/contenido_model.dart';
import 'package:cfca_apps/models/modulo_model.dart';

import 'package:cfca_apps/repositories/contenido_repository.dart';
import 'package:cfca_apps/repositories/modulo_repository.dart';
import 'package:flutter_html/style.dart';
//import 'package:path_provider/path_provider.dart';
//import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
//import 'package:youtube_explode_dart/youtube_explode_dart.dart';
//import 'package:path/path.dart' as p;

class LeccionPage extends StatefulWidget {
  final Curso? curso;
  final int? swiperIndex;
  final List<Contenido>? content;
  final Contenido? item;
  const LeccionPage({Key? key, this.curso, this.swiperIndex, this.content, this.item}) : super(key: key);

  @override
  _LeccionPageState createState() => _LeccionPageState();
}

class _LeccionPageState extends State<LeccionPage> {

  Curso? cursito;
  List<Modulo> modulo = [];
  List<Contenido> _contenidos = [];
  List<int> idsModulos = [];
  SwiperController _controller = new SwiperController();
  bool botonFin = true;

  //bool downloading = false;
  //var progressString = "";
  //String? urlVideo;
  //File? fileDir;

  //VideoPlayerController? _videoPlayerController2;
  // ignore: unused_field
  //ChewieController? _chewieController;

   @override
  void initState() {
    super.initState();
    setState(() {});
    _getObjects();
  }

  _getObjects() async {
    cursito = widget.curso;
    List<Modulo> modulos = await ModuloRepository().getAllModulos(cursito!.id as int);

    modulos.forEach((item) {
      idsModulos.add(item.id as int);
    });

    List<Contenido> cont =
        await ContenidoRepository().getAllContenidos(idsModulos);

    setState(() {
      modulo = modulos;
      _contenidos = cont;
    });
  }

  // Future<String> downloadVideo(String link, String name) async {
  //   final result = "/algo/"; //await FlutterYoutubeDownloader.downloadVideo(
  //       //link, "$name", 18);
  //   return result;
  // }

  // Future<void> descargaVideo(String id, String nameVideo, int sWindex ) async {
  //   var yt = YoutubeExplode();
  //   var video = await yt.videos.get(id);

  //   // Display info about this video.
  //   await showDialog(
  //     context: context,
  //     barrierDismissible: false,
  //     builder: (context) {
  //       return AlertDialog(
  //         content: Text('Title: ${video.title}, Duration: ${video.duration}'),
  //         actions: <Widget>[
  //           TextButton(
  //             child: const Text('Cerrar'),
  //             onPressed: () {
  //               Navigator.of(context).pop();
  //             },
  //           ),
  //         ],
          
  //       );
  //     },
  //   );

  //   // Request permission to write in an external directory.
  //   // (In this case downloads)
  //   await Permission.storage.request();

  //   // Get the streams manifest and the audio track.
  //   var manifest = await yt.videos.streamsClient.getManifest(id);
  //   var audio = manifest.muxed.first;

  //   // Build the directory.
  //   var dir = await getApplicationDocumentsDirectory();
  //   var filePath = p.join(dir.uri.toFilePath(), '${video.id}.${audio.container.name}');

  //   // Open the file to write.
  //   var file = File(filePath);
  //   var fileStream = file.openWrite();

  //   // Pipe all the content of the stream into our file.
  //   var audioStream = yt.videos.streamsClient.get(audio);
  //   // Track the file download status.
  //   // ignore: unused_local_variable
  //   var len = audio.size.totalBytes;
  //   // ignore: unused_local_variable
  //   var count = 0;

  //   await for (final data in audioStream) {
  //     count += data.length;

  //     fileStream.add(data);

  //   }

  //   // Close the file.
  //   await fileStream.flush();
  //   await fileStream.close();

  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString('video_$nameVideo', '$filePath');
  //   print(prefs.getString('video_$nameVideo'));
   
  //   final File fileDir = File('$filePath');
  //   _videoPlayerController2 = VideoPlayerController.file(fileDir);
  //   await Future.wait([_videoPlayerController2!.initialize()]);
  //   _chewieController = ChewieController(
  //     videoPlayerController: _videoPlayerController2!,
  //     autoPlay: false,
  //     looping: false,
  //     aspectRatio: 16 / 9,
  //   );

  //   setState(() {
  //   });
  //   _controller.move(sWindex);
  // }


  @override
  Widget build(BuildContext context) {
    _controller.move(widget.swiperIndex as int);
    final detalleLeccion = widget.item;
    List<Contenido>? menuLecciones = widget.content;
    return SafeArea(
      child: Scaffold(
         backgroundColor: Color(0xFFFFFFFF),
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black26),
            onPressed: () {
              Navigator.pop(context, false);
            }
          ), 
          centerTitle: true,
          title: Text('', style: TextStyle(color: Color(0xFF7C7C7C)),),
          iconTheme: IconThemeData(color: Colors.black26),
          backgroundColor: Color(0xFFFFFFFF),
          elevation: 0.0,
        ),
        body: _swiper(detalleLeccion),
        floatingActionButton: Container(
          child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomLeft,
                  child: FloatingActionButton.extended(
                    icon: Icon(Icons.arrow_back_outlined),
                    label: Text("Anterior"),
                    heroTag: null,
                    onPressed: () { 
                      _controller.previous();
                     },
                    elevation: 0.0,
                    backgroundColor: Color(0xFF2B4A6F)
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: FloatingActionButton(
                    child: Icon(Icons.navigation),
                    heroTag: null,
                    backgroundColor: Color(0xFF2B4A6F),
                    onPressed: () { 
                      showModalBottomSheet(
                          isScrollControlled: true,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20)
                            )
                          ),
                          context: context, 
                          builder: (context){
                            return Container(
                              height: MediaQuery.of(context).size.height * 0.7,
                              child: ListView.builder(
                                itemCount: menuLecciones!.length,
                                itemBuilder: (context, index){
                                  return ListTile(
                                    title: Text(menuLecciones[index].titulo as String),
                                    onTap: (){
                                      _controller.move(index);
                                      Navigator.pop(context,false);
                                    },
                                  );
                                }
                              ),
                            );
                          }
                        );
                     },),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton.extended(
                    
                    icon: Icon(Icons.arrow_forward_outlined),
                    heroTag: null,
                    onPressed: () {
                      if (botonFin == true) {
                        _controller.next();
                      } else {
                        AwesomeDialog(
                            context: context,
                            animType: AnimType.SCALE,
                            dialogType: DialogType.SUCCES,
                            body: Center(child: Padding(
                              padding: const EdgeInsets.all(11.0),
                              child: Text(
                                      'Felicidades terminastes el curso!!!',
                                      style: TextStyle(fontStyle: FontStyle.normal,
                                                      fontSize: 20,
                                                      fontWeight: FontWeight.bold,
                                                      ),
                                    ),
                            ),),
                            title: 'No titulo',
                            desc:   'No descripcion',
                            btnOkOnPress: () {
                              Navigator.pushNamed(context, 'aprende');
                            },
                        )..show();
                      }
                    }, 
                    label: Text("Siguiente"),
                    elevation: 0.0,
                    backgroundColor: Color(0xFF2B4A6F)
                  ),
                ),
              ],
            ),
        ),
        bottomNavigationBar: BottomAppBar(
          color: Color(0xFF2B4A6F),
          child: Container(height: 48),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }

  Widget _swiper(detalleLeccion) {
    return Swiper(
      controller: _controller,
      itemBuilder: (BuildContext context, int index) {
        //print("index $index / ${_contenidos.length}");
        return _contenidoLeccion(detalleLeccion, index);
      },
      loop: false,
      itemCount: _contenidos.length,
      onIndexChanged: (int index){
        var conteo = index + 1;
        if ( conteo == _contenidos.length ) {
            botonFin = false;
        } else {
          botonFin = true;
        }
        
      },
      
    );
  }

  Widget _contenidoLeccion(Contenido detalleLeccion, int index) {
    //print("index $index / ${_contenidos.length}");
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15.0),
            child: Text(_contenidos[index].titulo as String,
                  style: TextStyle(fontSize: 24.0,fontWeight: 
                          FontWeight.bold, color: 
                          Color(0xFF2B4A6F))
            )
          ),
          Divider(height: 5,thickness: 3,),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Html(
              data: _contenidos[index].contenido,
              style: {
                "html": Style(
                  fontSize: FontSize(16),
                ),
              },
              customRender: {
                "img": (RenderContext context, Widget child)  {
                  String _imgBody = context.tree.element!.attributes['src'] as String;
                  return InteractiveViewer(
                    child: CachedNetworkImage(
                      imageUrl: _imgBody,
                      placeholder: (context, url) => Center(child: CircularProgressIndicator(),),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  );
                },
              },
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal:30),
              child: (_contenidos[index].urlVideo != null)
              ? Column(children: [
                Text(_contenidos[index].nombreVideo as String, style: TextStyle(fontSize: 18),),
              ])
              :Text("")
          ),
          
          Container(
            child: (_contenidos[index].urlVideo != null) 
              ? FutureBuilder(
                  future: _comprobarVideo('${_contenidos[index].id}'),
                  //initialData: InitialData,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if ( snapshot.hasData) {
                      
                      if ( snapshot.data[0] ) {
                        
                        return Container(
                          width: 500,
                          height: 200,
                          child: VideoHome(
                            looping: false,
                            videoPlayerController: VideoPlayerController.file(File(snapshot.data[1]))),
                        );


                      }
                      else {
                        return DownloadVideo(videoString: _contenidos[index].urlVideo.toString(), videoName: _contenidos[index].id.toString());
                      }
                    } else {
                      print("NO hay video");
                    }
                    return Container();
                  },
                )
              : Text("")
          ),
          
          SizedBox(height: 15,)
          
          
        ],
      )
    );
  }

  _comprobarVideo(String videoName) async {
    final prefs = await SharedPreferences.getInstance();
    bool existe = false;
    //print("******tiene video*********");
    //print(prefs.getString('video_$videoName'));
    var ruta = prefs.getString('video_$videoName');
    if ( ruta != null ) {
      existe = true;
    } else {
      existe = false;
    }
    return [existe, ruta];
  }

  
}