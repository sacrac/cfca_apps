import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cfca_apps/models/directorio_model.dart';

class DirectorioDetail extends StatelessWidget {
  const DirectorioDetail({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final Directorio todo = ModalRoute.of(context)!.settings.arguments as Directorio;
    const stylesD = TextStyle(color: Colors.black54,
                              fontSize: 16,
                              fontWeight: FontWeight.bold);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Información del contacto", style: TextStyle(color: Color(0xFF7C7C7C)),),
        iconTheme: IconThemeData(color: Color(0xFF7C7C7C)),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0.0,
      ),
      backgroundColor: Color(0xFFFFFFFF),
      body: SingleChildScrollView(
        child: Stack(
          children: [
                Container(
                  width: double.infinity,
                  height: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/images/fondo_directorio.jpg'
                      ),
                      fit: BoxFit.cover
                    )
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 150, vertical: 150),
                  child: Hero(
                    tag: '${todo.id}',
                    child: CircleAvatar(
                        radius: 50.0,
                        backgroundImage:
                          AssetImage('assets/images/${todo.temaSlug}.png'),
                        backgroundColor: Colors.white,
                      ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  //color: Colors.white,
                  margin: const EdgeInsets.only(top: 300),
                  child:  Column(
                  children: [
                    Center(child: Text('${todo.nombrePersona}', style: stylesD)),
                    SizedBox(height: 10),
                    Center(child: Text('${todo.municipio}', style: stylesD)),
                    SizedBox(height: 10),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Center(
                        // ignore: unnecessary_null_comparison
                        child: Text( ({todo.direccion} != null) ? '${todo.direccion}' : 'Sin Dirección',style: stylesD)
                      ),
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xFF7DAB82),
                            padding: EdgeInsets.all(15),
                            textStyle: TextStyle(
                              fontSize: 17,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0)
                            ),
                            minimumSize: Size(150, 10)
                          ),
                          onPressed: (){
                            if (todo.telefono != null) {
                              launch("tel:+505${todo.telefono}");
                            }else if (todo.oficina != null) {
                              launch("tel:+505${todo.oficina}");
                            } else {
                              AlertDialog alert = AlertDialog(
                              title: Text("Usuario sin Teléfono"),
                              content: Text("Este Usuario no tiene ningun número telefonico"),
                              actions: [
                                TextButton(
                                  child: Text('Bien!'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                              );
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                },
                              );
                            }
      
                          },
                          child: Text("Llamar"),
                        ),
                        SizedBox(width: 25),
                        ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Color(0xFF7678AC),
                          padding: EdgeInsets.all(15),
                          textStyle: TextStyle(
                            fontSize: 17,
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0)
                          ),
                          minimumSize: Size(150, 10)
                        ),
                        onPressed: () {
                          if(todo.email != null){
                            final Uri _emailLaunchUri = Uri(
                              scheme: 'mailto',
                              path: '${todo.email}',
                              queryParameters: {
                                'subject': 'Nuevo Correo'
                              }
                            );
                            launch(_emailLaunchUri.toString());
                          } else {
                            AlertDialog alert = AlertDialog(
                              title: Text("Usuario sin Correo"),
                              content: Text("Este Usuario no tiene correo"),
                              actions: [
                                TextButton(
                                  child: Text('Bien!'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return alert;
                              },
                            );
                          }
                        },
                        child: Text("Enviar correo"),
                      ),
                      ]
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 20),
                      child: Divider(thickness: 2,)
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Teléfono: ", style: stylesD),
                          Spacer(),
                          Text( (todo.telefono != null) ? '${todo.telefono}' : 'Sin Teléfono',style: stylesD, )
                        ]
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Teléfono oficina: ", style: stylesD),
                          Spacer(),
                          Text( (todo.oficina != null) ? '${todo.oficina}' : 'Sin Teléfono',style: stylesD, )
                        ]
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Clasificación: ", style: stylesD),
                          Spacer(),
                          RichText( text: TextSpan(text: (todo.tema != null) ? '${todo.tema}' : 'Sin Clasificación',style: stylesD) )
                          
                        ]
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [Text("Correo:", style: stylesD)]),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          RichText( text: TextSpan(text: (todo.email != null) ? '${todo.email}' : 'Sin Correo',style: stylesD) )
                        ]
                      ),
                    )
                  ],
                )
                )
      
          ],
        ),
      ),
    );
  }
}




// Positioned(
//   child: Align(
//     alignment: Alignment.bottomCenter,
//     child: CircleAvatar(
//       radius: 60.0,
//       backgroundImage:
//           NetworkImage('https://via.placeholder.com/150'),
//       backgroundColor: Colors.transparent,
//     ),
//   ),
// ),