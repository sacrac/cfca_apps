import 'package:flutter/material.dart';

class InformacionPage extends StatelessWidget {
  const InformacionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            backgroundColor: Color(0xFF2B4A6F),
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text("Acerca de", style: TextStyle(color: Color(0xFFFFFFFF), fontWeight: FontWeight.bold),),
              background: FadeInImage(
                placeholder: AssetImage("assets/images/foto_acerca.jpg"),
                image: AssetImage("assets/images/foto_acerca.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverList(delegate: SliverChildListDelegate(
            [
            Column(
              children: [
              Container(
              //color: Colors.blueAccent,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: 'El Proyecto Tecnología para la Gestión Sostenible del Recurso Hídrico ',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                      children: <TextSpan>[
                        TextSpan(text: '(TGSRH), ',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),

                        ),
                        TextSpan(
                            text: ' es una iniciativa promovida y financiada por Asuntos Globales de Canadá ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: '(GAC),',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),

                        ),
                        TextSpan(
                            text: ' la Fundación Cambio para los Niños ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: '(CFCA) ',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' y ejecutado por el Servicio de Información Mesoamericano sobre Agricultura Sostenible ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: '(SIMAS) ',
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                      ]
                  ),
                )
              )
            ),
            Container(
              //color: Colors.orangeAccent,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 6),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: 'El propósito del proyecto, es Fortalecer a los Comités de Agua y Saneamiento con un mejor uso de tecnología por parte de las poblaciones rurales para gestionar procesos democráticos de toma de decisión en la gobernanza del agua.',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),

                  ),
                )
              )
            ),
            Container(
              //color: Colors.purpleAccent,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: 'El proyecto interviene en 13 municipios de los departamentos de León y Chinandega. En ',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                      children: <TextSpan>[
                        TextSpan(text: 'León, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),

                        ),
                        TextSpan(
                            text: ' cubriendo los municipios de  ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: 'León,',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' Telica, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' Quezalguaque, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' El Sauce, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' Larreynaga, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' El Jicaral, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' y ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: 'Achuapa. ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' En Chinandega en los municipios de  ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: 'Chinandega, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: 'Somotillo, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: 'Villanueva, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: 'Chichigalpa, ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: 'Posoltega ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                        TextSpan(
                            text: ' y ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: 'Santo Tomás del Norte ',
                            style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                      ]
                  ),
                )
              )
            ),
            Container(
              //color: Colors.tealAccent,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: 'Con la App-CAPS Nicaragua, el proyecto corresponde el apoyo técnico para mejorar habilidades en uso de las tecnologías, temas de organización, administración, género y mantenimiento de los sistemas de agua, dirigida a representantes de las Juntas Directivas de los  ',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                      children: <TextSpan>[
                        TextSpan(text: 'CAPS. ',
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),

                        ),
                        TextSpan(
                            text: ' Al igual que generar un espacio de peticiones para facilitar la interacción entre gobiernos locales, nacionales y   ',
                            style: TextStyle(
                                //color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),
                        TextSpan(
                            text: 'CAPS.',
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                //decoration: TextDecoration.underline
                            ),
                        ),
                      ],
                  ),
                )
              )
            ),
            SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Image.asset("assets/images/todologos.png")),
            SizedBox(height: 10),
            ],
          ),
            ]
          ))
        ]
      )
    );
  }
}