import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HazPeticionPage extends StatefulWidget {

  @override
  _HazPeticionPageState createState() => _HazPeticionPageState();
}

class _HazPeticionPageState extends State<HazPeticionPage> {
  bool isLoading=true;

  final _key = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Haz tu petición", style: TextStyle(color: Color(0xFF7C7C7C)),),
        iconTheme: IconThemeData(color: Color(0xFF7C7C7C)),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0.0,
      ),
      body: Stack(
        children: <Widget>[
          WebView(
            key: _key,
            initialUrl: 'https://caps-nicaragua.org/peticiones/make/',
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (finish) {
              setState(() {
                isLoading = false;
              });
            },
          ),
          isLoading ? Center( child: CircularProgressIndicator(),)
                    : Stack(),
        ],
      ),
    );
  }
}



// WebView(
//         initialUrl: 'https://caps-nicaragua.org/peticiones/make/',
//         javascriptMode: JavascriptMode.unrestricted,
//       ),