import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cfca_apps/blocs/directorio/directorio_bloc.dart';
import 'package:cfca_apps/searching/search_directorio.dart';
import 'package:cfca_apps/models/directorio_model.dart';
import 'package:cfca_apps/widgets/drawer_widget.dart';

class DirectorioPage extends StatefulWidget {
  //final UserBLoC userBLoC = new UserBLoC();
  @override
  _DirectorioPageState createState() => _DirectorioPageState();
}

class _DirectorioPageState extends State<DirectorioPage> {


  @override
  void initState() {
    super.initState();
    _loadDirectorio();
  }


  _loadDirectorio() async {
    BlocProvider.of<DirectorioBloc>(context).add(DirectorioRequest());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      appBar: AppBar(
        centerTitle: true,
        title: Text("Directorio", style: TextStyle(color: Color(0xFF7C7C7C)),),
        iconTheme: IconThemeData(color: Color(0xFF7C7C7C)),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0.0,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              showSearch(context: context, delegate: BuscadorPersona('Buscar...') );
            }
        )
        ],
        // actions: [
        //   Chip(
        //     label: StreamBuilder<int>(
        //         stream: userBLoC.userCounter,
        //         builder: (context, snapshot) {
        //           return Text(
        //             (snapshot.data ?? 0).toString(),
        //             style: TextStyle(
        //                 color: Colors.white, fontWeight: FontWeight.bold),
        //           );
        //         }),
        //     backgroundColor: Colors.red,
        //   ),
        //   Padding(
        //     padding: EdgeInsets.only(right: 16),
        //   )
        // ],
      ),
      drawer: menuDraver(context),
      body: SingleChildScrollView(
        child: Column(
          children: [
          BlocBuilder<DirectorioBloc, DirectorioState>(
            builder: ( _ , state) {
            //print(state);
              if (state is DirectorioLoadFailure) {
                return Center(child: Text("Hubo un Error"));
              }
              if (state is DirectorioLoadSuccess) {
                final directorio = state.directorio;
                return DirectorioList(directorios: directorio);
              }
              return Center(child: CircularProgressIndicator());
            },
          )
          ],
        ),
      )
    );
  }
}


class DirectorioList extends StatelessWidget {
  final List<Directorio> directorios;

  DirectorioList({Key? key, required this.directorios}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: directorios.length,
      itemBuilder: (context, index) {
        //print("${directorios[index].nombrePersona}");
        return Card(
          child: ListTile(
          leading: Hero(
            tag: '${directorios[index].id}',
            child: CircleAvatar(
                      radius: 30.0,
                      backgroundImage:
                        AssetImage('assets/images/${directorios[index].temaSlug}.png'),
                      backgroundColor: Colors.transparent,
                    ),
          ),
          title: Text("${directorios[index].nombrePersona}",
                      style: TextStyle(
                        fontSize: 19.0,
                        fontWeight: FontWeight.w400
                      ),
                  ),
          subtitle: Text("${directorios[index].tema}, ${directorios[index].municipio}",
                                style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 16.0
                                ),
                            ),
          onTap: (){
            Navigator.pushNamed(context, 'directorio_detail',
            arguments: directorios[index]);
          },
          trailing: Icon(Icons.keyboard_arrow_right),
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          //selected: true,
        ),
        );
      },
    );
  }
}