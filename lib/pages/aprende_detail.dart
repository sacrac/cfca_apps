import 'package:cfca_apps/dao/contenido_dao.dart';
import 'package:cfca_apps/models/contenido_model.dart';
import 'package:cfca_apps/models/modulo_model.dart';
import 'package:cfca_apps/pages/leccion_detail.dart';
import 'package:cfca_apps/repositories/contenido_repository.dart';
import 'package:cfca_apps/repositories/modulo_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/repositories/curso_repository.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:flutter_html/style.dart';

class AprendeDetalle extends StatefulWidget {

  @override
  _AprendeDetalleState createState() => _AprendeDetalleState();
}

class _AprendeDetalleState extends State<AprendeDetalle> {
  
  @override
  Widget build(BuildContext context) {

    final List cursoDetalle = ModalRoute.of(context)!.settings.arguments as List;
    int cursoid = cursoDetalle[0];
    String cursoName = cursoDetalle[1];
    List<int> idsModulos = [];
    final cursoNombre = cursoName;
    final Curso cursos = cursoDetalle[2];

    Future getFilterModulos() async {
      List<Modulo> filterModulos = [];
      List<Contenido> filterContenidos = [];

      filterModulos = await ModuloRepository().getAllModulos(cursoid);
      filterModulos.forEach((item) {
          idsModulos.add(item.id as int);
      });

      filterContenidos = await ContenidoRepository().getAllContenidos(idsModulos);

      return [filterModulos, filterContenidos];
    }

    return FutureBuilder(
      future: getFilterModulos(),
      //initialData: InitialData,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          var listModulos = snapshot.data[0];
          var listContenido = snapshot.data[1];
          
          return Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text(cursoNombre, style: TextStyle(color: Color(0xFF7C7C7C)),),
              iconTheme: IconThemeData(color: Colors.black26),
              backgroundColor: Color(0xFFFFFFFF),
              elevation: 0.0,
            ),
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  introducionCurso(cursoid, context),
                  modulosCurso(listModulos, listContenido, context, cursos)
                ],
              )
            ),
          );
        } else {
        }
        return Center(child: CircularProgressIndicator());
      },
    );

  }

  Widget introducionCurso(int cursoid, BuildContext context ){
    var width = MediaQuery.of(context).size.width;
    //var height = MediaQuery.of(context).size.height;
    final cursoAll = CursoRepository().getCursoId(cursoid);
    return SingleChildScrollView(
      child: FutureBuilder(
        future: cursoAll,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if ( snapshot.hasData ) {
            final Curso curso = snapshot.data;
            return Column(
              children: [
                Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      child: CachedNetworkImage(imageUrl: curso.imagen as String, fit: BoxFit.cover,)),
                    Positioned.fill(
                      child: Column(
                        children: [
                          Expanded(child: Container()),
                          Container(
                          width: width * 0.9,
                          height: 80,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Color(0xFF2B4A6F),
                              padding: EdgeInsets.all(15),
                              textStyle: TextStyle(
                                fontSize: 21,
                                fontWeight: FontWeight.bold
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0)
                              )
                            ),
                            onPressed: (){
                              showModalBottomSheet(
                                isScrollControlled: true,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(15)
                                  )
                                ),
                                context: context,
                                builder: (context) {
                                  return GestureDetector(
                                    onTap: () => Navigator.of(context).pop(),
                                    child: Container(
                                      height: MediaQuery.of(context).size.height * 0.7,
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Center(child: Text("Introducción", style: TextStyle(color: Color(0xFF2B4A6F), fontSize: 23))),
                                            Html(
                                              data: curso.descripcion,
                                              style: {
                                                "html": Style(
                                                  fontSize: FontSize(16)
                                                ),
                                              },
                                              customRender: {
                                                "img": (RenderContext context, Widget child)  {
                                                  String _imgBody =  context.tree.element!.attributes['src'] as String;
                                                  
                                                  return CachedNetworkImage(
                                                    imageUrl: _imgBody,
                                                    placeholder: (context, url) => Center(child: CircularProgressIndicator(),),
                                                    errorWidget: (context, url, error) => Icon(Icons.error),
                                                  );
                                                },
                                              },
                                            ),
                                            SizedBox(height:10.0)
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              );
                            }, 
                            child: Text("Introducción"),
                          ),
                        ),
                        ],
                      ),
                    ),
                ],
              ),
            ],
          );
          } else {
            return Center(child: CircularProgressIndicator());
          }

        },
        )
    );
  }

  _conteo(int id) async {
    int x = await ContenidoDao().conteoContenido(id);
    return x;
  }

  Widget modulosCurso(List modulo, List<Contenido> contenido, BuildContext context, Curso curso) {
    
    return SingleChildScrollView(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: modulo.length,
        padding: EdgeInsets.only(bottom: 5.0),
        controller: ScrollController(keepScrollOffset: false),
        itemBuilder: (context, index) {
          final cardCurso = Card(
            //color: Color(0xFF2B4A6F),
            child: ExpansionTile(
            title: Text('${modulo[index].titulo}'),
            subtitle: FutureBuilder(
              future: _conteo(modulo[index].id),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return Text(
                    snapshot.data.toString() + ' Lecciones',
                      style: TextStyle(color: Color(0xFF2B4A6F)),
                  );
                } else {
                  return Text('');
                }
              },
            ),
              children: [
                Column(
                  children: <Widget>[
                    _lecciones(modulo[index].id, contenido, context, curso)
                  ],
                    ),]
                ));
                return GestureDetector(
                  child: cardCurso,
                  onTap: () => null,
                );
            },
            
        ),
    );
  }

  Widget _lecciones(int idmodulo, List<Contenido> contenido, BuildContext context, Curso curso) {
    List<Widget> lisItem = [];
    for (var item in contenido) {
        if (item.moduloId == idmodulo) {
            lisItem.add(
                GestureDetector(
                    child:  Card(
                        child: ListTile(
                          trailing: Icon(Icons.arrow_forward),
                          title: Text(item.titulo as String),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    LeccionPage(curso: curso, 
                                                item: item, 
                                                content: contenido,
                                                swiperIndex: contenido.indexOf(item))),
                          );
                    } 
                )
            );

        }
    }
    return Column(children:lisItem,);
  }



}