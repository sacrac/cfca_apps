import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MapaPage extends StatefulWidget {
  const MapaPage({Key? key}) : super(key: key);

  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  bool isLoading=true;

  final _key = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Cobertura CAPS", style: TextStyle(color: Color(0xFF7C7C7C)),),
        iconTheme: IconThemeData(color: Color(0xFF7C7C7C)),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0.0,
      ),
      body: Stack(
        children: <Widget>[
          WebView(
            key: _key,
            initialUrl: 'https://caps-nicaragua.org/caps/mobile-mapa/',
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (finish) {
                setState(() {
                  isLoading = false;
                });
              },
          ),
          isLoading ? Center( child: CircularProgressIndicator(),)
                    : Stack(),
        ]
      ),
    );
  }
}