
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cfca_apps/dao/contenido_dao.dart';
import 'package:cfca_apps/dao/modulo_dao.dart';
import 'package:cfca_apps/searching/search_cursos.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:cfca_apps/blocs/cursos/cursos_bloc.dart';
import 'package:cfca_apps/models/curso_model.dart';
import 'package:cfca_apps/widgets/drawer_widget.dart';

class AprendePage extends StatefulWidget {

  @override
  _AprendePageState createState() => _AprendePageState();
}

class _AprendePageState extends State<AprendePage> {


  @override
  void initState() {
    super.initState();
    _loadCursos();
  }


  _loadCursos() async {
    BlocProvider.of<CursosBloc>(context).add(CursosRequest());
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      appBar: AppBar(
        centerTitle: true,
        title: Text("Cursos", style: TextStyle(color: Color(0xFF7C7C7C)),),
        iconTheme: IconThemeData(color: Colors.black26),
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0.0,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              showSearch(context: context, delegate: BuscadorCurso('Buscar...') );
            }
        )
        ],
      ),
      drawer: menuDraver(context),
      body: BlocBuilder<CursosBloc, CursosState>(
            builder: ( _ , state) {
            //print(state);
              if (state is CursoLoadFailure) {
                return Center(child: Text("Hubo un Error"));
              }
              if (state is CursoLoadSuccess) {
                final cursos = state.curso;
                return CursosList(cursos: cursos);
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
    );
  }
}

_conteoModulos(int id) async {
  int x = await ModuloDao().conteoModulosPages(id);
  return x;
}

_conteoContenidos(int id) async {
  int x = await ContenidoDao().conteoContenidosPages(id);
  return x;
}

class CursosList extends StatelessWidget {
  final List<Curso> cursos;
  const CursosList({Key? key, required this.cursos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: cursos.length,
      primary: false,
      shrinkWrap: true,
      itemBuilder: (context, index){
      return Card(
        color: Color(0xFFF2F2F2),
        clipBehavior: Clip.antiAlias,
        elevation: 2.0,
        child: Column(
            children: [
              Stack(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.pushNamed(context, 'aprende_detail', arguments: [cursos[index].id, cursos[index].titulo, cursos[index]]);
                    },
                    child: CachedNetworkImage(
                      key: UniqueKey(),
                      imageUrl: cursos[index].imagen as String, 
                      fit: BoxFit.cover, 
                      width: 1000.0,
                      placeholder: (context, url) => Center(child: CircularProgressIndicator(),),
                      errorWidget: (context, url, error) => Icon(Icons.error)
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.all(16.0).copyWith(bottom:0),
                child: Text(
                  '${cursos[index].titulo}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF2B4A6F),
                    fontSize: 17,
                  ),
                ),
              ),
              Divider(color: Color(0xFF2B4A6F),),
              SizedBox(height: 8),
              Row(
                children: [
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FutureBuilder(
                        future: _conteoModulos(cursos[index].id as int),
                        builder: (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return Chip(
                              labelPadding: EdgeInsets.all(3.0),
                              avatar: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Text(snapshot.data.toString(),
                                style: TextStyle(
                                  color: Color(0xFFAA7E38),
                                  fontWeight: FontWeight.bold),),
                              ),
                              label: Text(
                                "TEMAS",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              backgroundColor: Color(0xFF2B4A6F),
                              //elevation: 6.0,
                              shadowColor: Colors.grey[60],
                              padding: EdgeInsets.all(8.0),
                            );
                          } else {
                            return Text('');
                          }
                        },
                      ),
                      FutureBuilder(
                        future: _conteoContenidos(cursos[index].id as int),
                        builder: (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return Chip(
                              labelPadding: EdgeInsets.all(3.0),
                              avatar: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Text(snapshot.data.toString(),
                                style: TextStyle(
                                  color: Color(0xFFAA7E38),
                                  fontWeight: FontWeight.bold)),
                              ),
                              label: Text(
                                "Lecciones",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              backgroundColor: Color(0xFF2B4A6F),
                              //elevation: 6.0,
                              shadowColor: Colors.grey[60],
                              padding: EdgeInsets.all(8.0),
                            );
                          } else {
                            return Text('');
                          }
                        },
                      ),
                    ],
                  ),
                  Spacer(),
                  ButtonBar(
                    alignment: MainAxisAlignment.end,
                    children: [
                      TextButton(onPressed: (){
                        Navigator.pushNamed(context, 'aprende_detail', arguments: [cursos[index].id, cursos[index].titulo, cursos[index]]);
                      }, child: Text("TOMAR CURSO", 
                      style: TextStyle(color: Color(0xFFAA7E38),
                      fontWeight: FontWeight.bold),))
                    ],
                  )
                ],
              )
              
            ],
          ),
        );
      },

    );
  }
}

class CursoList extends StatelessWidget {
  final List<Curso> cursos;

  CursoList({Key? key, required this.cursos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: cursos.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
          leading: CachedNetworkImage(
              imageUrl: '${cursos[index].imagen}',
              imageBuilder: (context, imageProvider) => Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: imageProvider, fit: BoxFit.cover),
                ),
              ),
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          title: Text("${cursos[index].titulo}",
                      style: TextStyle(
                        fontSize: 19.0,
                        fontWeight: FontWeight.w400
                      ),
                  ),
          onTap: (){
            Navigator.pushNamed(context, 'aprende_detail',
            arguments: [cursos[index].id, cursos[index].titulo, cursos[index]]);
          },
          trailing: Icon(Icons.keyboard_arrow_right),
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
        ),
        );
      },
    );
  }
}