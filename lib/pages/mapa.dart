import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class MapaInfoPage extends StatefulWidget {
  const MapaInfoPage({Key? key}) : super(key: key);

  @override
  _MapaInfoPageState createState() => _MapaInfoPageState();
}

class _MapaInfoPageState extends State<MapaInfoPage> {
  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;


  @override
  initState() {
    super.initState();
     initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }


  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
      return;
    }
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }


  @override
  Widget build(BuildContext context) {
    final styleD = TextStyle(fontSize: 19, color: Color(0xFF2B4A6F));
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            backgroundColor: Color(0xFF2B4A6F),
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text("Mapa Cobetura", style: TextStyle(color: Color(0xFFFFFFFF), fontWeight: FontWeight.bold),),
              background: FadeInImage(
                placeholder: AssetImage("assets/images/mapa_page.jpg"),
                image: AssetImage("assets/images/mapa_page.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverList(delegate: SliverChildListDelegate(
            [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(20),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                    child: Column(
                      children: [
                        Text("Un Mapa interactivo en donde encontrarás información básica de 365 CAPS de los municipios de León y Chinandega que han sido apoyados por el proyecto TGSRH y conoce si cuentan con su legalidad municipal y nacional, al igual que las inversiones realizadas -perforaciones y rehabilitaciones a sistemas de agua. Para este mapa se destacan filtros con información que se requiera del CAPS:", style: styleD,),
                        SizedBox(height: 10,),
                        Text("Departamento y Municipio: Selecciona el lugar qué deseas filtrar entre 2 departamentos y 13 municipios. Tipo de sistema: Puedes saber cuántos CAPS tienen cuáles sistemas, entre MABE, MABE-ES, MABE- EC, MAG, PEBM, PPBM  Legalidad: Puedes conocer cuáles CAPS están legales a nivel municipal y nacional reconocidos por el ANA.", style: styleD,),
                        SizedBox(height: 10,),
                        Text("En el Mapa interactivo, podrás obtener el perfil completo de cada CAPS al seleccionar dentro de él la opción “Ver Ficha”.", style: styleD,)
                      ],
                    )
                  ),
                ),
                botonMapa(_connectionStatus.index),
                SizedBox(height: 20,),
              ],
            )
            ]
          ))
        ],
      ),
    );
  }

  Widget botonMapa(int index) {
    if ( index == 0 ) {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color(0xFF7DAB82),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          textStyle: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0)
          ),
        ),
        child: Text("ir a consultar mapa"),
        onPressed: (){
          Navigator.pushNamed(context, 'mapa');
        },
      );
    } else if (index == 1) {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color(0xFF7DAB82),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          textStyle: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
          shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0)
        ),
        ),
        child: Text("ir a consultar mapa"),
        onPressed: (){
          Navigator.pushNamed(context, 'mapa');
        },
      );
    } else if ( index == 2 ) {
      return Center(child: Text("Necesita conección a Internet para ver mapa"));
    } else {
      return Center(child: Text("Necesita conección a Internet para ver mapa"));
    }
  }

}