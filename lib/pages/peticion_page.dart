import 'dart:async';

import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';


class PeticionPage extends StatefulWidget {
  const PeticionPage({Key? key}) : super(key: key);

  @override
  _PeticionPageState createState() => _PeticionPageState();
}

class _PeticionPageState extends State<PeticionPage> {
  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  initState() {
    super.initState();
     initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
      return;
    }
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    final styleD = TextStyle(fontSize: 19);
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            backgroundColor: Color(0xFF2B4A6F),
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text("Peticiones", style: TextStyle(color: Color(0xFFFFFFFF), fontWeight: FontWeight.bold),),
              background: FadeInImage(
                placeholder: AssetImage("assets/images/agua_fondo.jpg"),
                image: AssetImage("assets/images/agua_fondo.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverList(delegate: SliverChildListDelegate(
            [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.orange[200],
                      borderRadius: BorderRadius.circular(20),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                    child: Text("En este espacio realiza peticiones a las ONGs y municipalidades de tu territorio en temas de: Auditoría, Mantenimiento, Mejoras a los sistemas de agua y conocimientos técnicos para el CAPS. ", style: styleD,)
                  ),
                ),
                botonPeticion(_connectionStatus.index)
                // BlocBuilder<NetworkBloc, NetworkState>(
                //   // ignore: missing_return
                //   builder: ( _ , state) {
                //     if ( state is ConnectionSuccess) {
                //       return ElevatedButton(
                //         style: ElevatedButton.styleFrom(
                //           primary: Color(0xFF7DAB82),
                //           padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                //           textStyle: TextStyle(
                //             fontSize: 20,
                //             fontWeight: FontWeight.bold
                //           ),
                //           shape: RoundedRectangleBorder(
                //           borderRadius: BorderRadius.circular(18.0)
                //         ),
                //         ),
                //         child: Text("Hacer una petición"),
                //         onPressed: (){
                //           Navigator.pushNamed(context, 'haz_peticion');
                //         },
                //       );
                //     } else {
                //       Center(child: Text("Necesita conección a Internet para hacer una petición"));
                //     }
                //     return Center(child: Text("Necesita conección a Internet para hacer una petición"));
                //   },
                // )
              ],
            )
            ]
          ))
        ],
      ),
    );
  }

  Widget botonPeticion(int index) {

    if ( index == 0 ) {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color(0xFF7DAB82),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          textStyle: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
          shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0)
        ),
        ),
        child: Text("Hacer una petición"),
        onPressed: (){
          Navigator.pushNamed(context, 'haz_peticion');
        },
      );
    } else if (index == 1) {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color(0xFF7DAB82),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          textStyle: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
          shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0)
        ),
        ),
        child: Text("Hacer una petición"),
        onPressed: (){
          Navigator.pushNamed(context, 'haz_peticion');
        },
      );
    } else if ( index == 2 ) {
      return Center(child: Text("Necesita conección a Internet para hacer una petición"));
    } else {
      return Center(child: Text("Necesita conección a Internet para hacer una petición"));
    }


  }
}