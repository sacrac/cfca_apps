class Contenido {
  int? id;
  int? moduloId;
  String? titulo;
  String? contenido;
  String? nombreVideo;
  String? urlVideo;
  int? order;
  int? modificado;

  Contenido(
      {this.id,
      this.moduloId,
      this.titulo,
      this.contenido,
      this.nombreVideo,
      this.urlVideo,
      this.order,
      this.modificado
      }
    );

  Contenido.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    moduloId = json['modulo_id'];
    titulo = json['titulo'];
    contenido = json['contenido'];
    nombreVideo = json['nombre_video'];
    urlVideo = json['url_video'];
    order = json['order'];
    modificado = json['modificado'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['modulo_id'] = this.moduloId;
    data['titulo'] = this.titulo;
    data['contenido'] = this.contenido;
    data['nombre_video'] = this.nombreVideo;
    data['url_video'] = this.urlVideo;
    data['order'] = this.order;
    data['modificado'] = this.modificado;
    return data;
  }
}