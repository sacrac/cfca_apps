class Modulo {
  int? id;
  int? cursoId;
  String? titulo;
  int? order;
  int? modificado;

  Modulo({this.id, this.cursoId, this.titulo, this.order, this.modificado});

  Modulo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cursoId = json['curso_id'];
    titulo = json['titulo'];
    order = json['order'];
    modificado = json['modificado'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['curso_id'] = this.cursoId;
    data['titulo'] = this.titulo;
    data['order'] = this.order;
    data['modificado'] = this.modificado;
    return data;
  }
}