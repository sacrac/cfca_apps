
class Directorios {

  List<Directorio> items = [];

  Directorios();

  Directorios.fromJsonList( List<dynamic> jsonList ) {

    // ignore: unnecessary_null_comparison
    //if ( jsonList == null ) return;

    for ( var item in jsonList ) {
      final directorio = new Directorio.fromJsonMap(item);
      items.add( directorio );
    }
  }

}

class Directorio {
  int? id;
  String? departamento;
  String? municipio;
  String? comunidad;
  String? tema;
  String? temaSlug;
  String? empresa;
  String? nombrePersona;
  String? direccion;
  int? telefono;
  int? oficina;
  String? email;
  int? modificado;

  Directorio({
    this.id,
    this.departamento,
    this.municipio,
    this.comunidad,
    this.tema,
    this.temaSlug,
    this.empresa,
    this.nombrePersona,
    this.direccion,
    this.telefono,
    this.oficina,
    this.email,
    this.modificado
  });


  Directorio.fromJsonMap( Map<String, dynamic> json) {

    id            = json['id'];
    departamento  = json['departamento'];
    municipio     = json['municipio'];
    comunidad     = json['comunidad'];
    tema          = json['tema'];
    temaSlug      = json['tema_slug'];
    empresa       = json['empresa'];
    nombrePersona = json['nombre_persona'];
    direccion     = json['direccion'];
    telefono      = json['telefono'];
    oficina       = json['oficina'];
    email         = json['email'];
    modificado    = json['modificado'];

  }

  Map<String, dynamic> toJson() => {
        "id"           : id,
        "departamento" : departamento,
        "municipio"    : municipio,
        "comunidad"    : comunidad,
        "tema"         : tema,
        "tema_slug"    : temaSlug,
        "empresa"      : empresa,
        "nombre_persona" : nombrePersona,
        "direccion"     : direccion,
        "telefono"     : telefono,
        "oficina"       : oficina,
        "email"        : email,
        "modificado"   : modificado
      };


}
