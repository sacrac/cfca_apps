
class ApiConstant {
  static const String aprende_cursos_api = 'cursos/api/v2/cursos/';
  static const String aprende_modulos_api = 'cursos/api/v2/modulos/';
  static const String aprende_contenidos_api = 'cursos/api/v2/contenidos/';
  static const String directorio_api = 'cursos/api/v2/directorio/';
}