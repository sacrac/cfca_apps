import 'package:cfca_apps/pages/home_page.dart';
import 'package:cfca_apps/pages/mapa.dart';
import 'package:flutter/material.dart';

import 'package:cfca_apps/pages/aprende_page.dart';
import 'package:cfca_apps/pages/directorio_page.dart';
import 'package:cfca_apps/pages/peticion_page.dart';

import 'package:cfca_apps/pages/aprende_detail.dart';
import 'package:cfca_apps/pages/leccion_detail.dart';
import 'package:cfca_apps/pages/directorio_detail.dart';

//import 'package:cfca_apps/pages/home_page.dart';
import 'package:cfca_apps/pages/peticion.dart';
import 'package:cfca_apps/pages/informacion_page.dart';
import 'package:cfca_apps/pages/mapa_page.dart';

Map<String, WidgetBuilder> getRouter() {
  return <String, WidgetBuilder> {
    'home'       : ( _ ) => HomePage(),
    'aprende' :    ( _ ) => AprendePage(),
    'aprende_detail' :    ( _ ) => AprendeDetalle(),
    'directorio' : ( _ ) => DirectorioPage(),
    'directorio_detail' : ( _ ) => DirectorioDetail(),
    'peticion'          : ( _ ) => PeticionPage(),
    'haz_peticion'      : ( _ ) => HazPeticionPage(),
    'mapa' : ( _ ) => MapaPage(),
    'mapa_info': ( _ ) => MapaInfoPage(),
    'info' : ( _ ) => InformacionPage(),
    'leccion' : ( _ ) => LeccionPage()
  };
}