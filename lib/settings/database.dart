import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBHelper {

    static final  DBHelper db = DBHelper._();//contructor privado es lo mismo que _private
    static Database? _database;

    DBHelper._();

    Future<Database?> get database async {
        if (_database != null) return _database;

        _database = await initDB();
        return _database;
    }
    void onUpgrade(Database database, int oldVersion, int newVersion) {
      if (newVersion > oldVersion) {}
    }
    initDB() async{
        Directory documentsDirectory = await getApplicationDocumentsDirectory();// obtener el path de donde se encuentra la bd
        // print("dir de la base de datos");
        // print(documentsDirectory.path);
        final path = join(documentsDirectory.path, 'cfca.db');

        return await openDatabase(
            path,
            version: 1,
            onUpgrade: onUpgrade,
            onOpen: (db){},
            onCreate: (Database db, int version) async{
                await db.execute(
                    'CREATE TABLE Curso('
                    'id INTEGER PRIMARY KEY,'
                    'titulo TEXT,'
                    'imagen TEXT,'
                    'descripcion TEXT,'
                    'fecha TEXT,'
                    'modificado INTEGER'
                    ')'
                );
                await db.execute(
                    'CREATE TABLE Modulo('
                    'id INTEGER PRIMARY KEY,'
                    'curso_id INTEGER,'
                    'titulo TEXT,'
                    '"order" INTEGER,'
                    'modificado INTEGER'
                    ')'
                );

                await db.execute(
                    'CREATE TABLE Contenido('
                    'id INTEGER PRIMARY KEY,'
                    'modulo_id INTEGER,'
                    'titulo TEXT,'
                    'contenido TEXT,'
                    '"order" INTEGER,'
                    'url_video TEXT,'
                    'nombre_video TEXT,'
                    'modificado INTEGER'
                    ')'
                );

                await db.execute(
                    'CREATE TABLE Directorio('
                    'id INTEGER PRIMARY KEY,'
                    'departamento TEXT,'
                    'municipio TEXT,'
                    'comunidad TEXT,'
                    'tema TEXT NULL,'
                    'tema_slug TEXT NULL,'
                    'empresa TEXT NULL,'
                    'nombre_persona TEXT,'
                    'direccion TEXT NULL,'
                    'telefono INTEGER NULL,'
                    'oficina INTEGER NULL ,'
                    'email TEXT NULL,'
                    'modificado INTEGER'
                    ')'
                );

            }
        );


    }
}